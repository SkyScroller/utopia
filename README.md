# Utopia

Utopia is a software that adds plugin support to the game "SCP: Secret Laboratory".
It's based on Atlas (Copyright © 2019 Luke Janik, MIT License).
Since SCP:SL published MP2 was Smod no longer working and the further development appeared to ended.

My goal with Utopia is to make the development of plugins possible again and make it more appealing than Smod did that.
Smod had also some limitations that that wasn't necessary.

## Features in comparence to SMod

| Feature | Utopia | SMod |
| ------ | ------ | ------ |
| Updates| ✔ Fast and easy| ✖ Full Serverupdate is needed |
| Full Documentation | ✔ | ✖ Incomplete|
| Usefull Example Files | ✔ | ✖ Outdated and confusing |
| Update, Add, Remove Plugins at Runtime without restart| ✔ | ✖ |
| Secure | ✔ Exploits from SCP will be fixed | ✖ some Exploits can't be fixed |
| Pricing | 5€ | Free |
| Free Demo Version | ✔ | ✔ |  
| Free Developer Version| ✔ | ✔ | 

[See all Utopia Feature's](https://gitlab.com/SkyScroller/utopia/-/wikis/features)


# Instalation & Troubleshooting

The installation and troubleshooting guide can be found [The Wiki](https://gitlab.com/SkyScroller/utopia/-/wikis/home)

# FAQ

For the FAQ please visit the [FAQ in the Wiki](https://gitlab.com/SkyScroller/utopia/-/wikis/faq)

# Support & Discussion

For further support or discussion you can join our discord. \
[https://discord.gg/AX3GnTN](https://discord.gg/AX3GnTN)

# Documentation

The documentation can be found at [The Wiki](https://gitlab.com/SkyScroller/utopia/-/wikis/home)

# Official Verified Plugins

Official verified plugins are selected plugins by developers that prove to be:

• stable \
• easy to configure and understand \
• support the english language \
• usefull and effective

**Official verified plugins**

Name of the Plugin | by Name of the Developer | Downloadlink

# Official Verified Developer

SkyScroller
Flo0205

# License
Atlas (Copyright © 2019 Luke Janik, MIT License) \
Utopia (Copyright © 2020 Dennis Hutorni, [Open License](LICENSE))